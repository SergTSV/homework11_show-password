'use strict';

const inputWrapper = Array.from(document.querySelectorAll('.input-wrapper'))

const btn = document.querySelector('.btn');

const inputLabel = document.querySelectorAll('.input-wrapper');
const icon = document.querySelectorAll('.icon-password');

const pwList = document.querySelector('.pwList');
const progressBarItem = document.querySelectorAll('.progress-bar_item');
const progressBarText = document.querySelector('.progress-bar_text');

btn.disabled = true;

icon.forEach(elemPassword => elemPassword.addEventListener('click', targetPassword => {

    if(targetPassword.target.offsetParent.children[0].getAttribute('type') === 'password') {
        targetPassword.target.offsetParent.children[0].setAttribute('type', 'text');
        targetPassword.target.classList.remove('fa-eye');
        targetPassword.target.classList.add('fa-eye-slash');
    } else {
        targetPassword.target.offsetParent.children[0].setAttribute('type', 'password');
        targetPassword.target.classList.add('fa-eye');
        targetPassword.target.classList.remove('fa-eye-slash');
    };
}));



btn.addEventListener('click', (e) => {
    e.preventDefault()
        if (inputWrapper[1].value === inputWrapper[0].value) {
            alert('Congratulations! You entered the password correctly!')
        } else {
            alert('Looks like something went wrong. Passwords do not match!');
        }
})

inputLabel.forEach(input => input.addEventListener('keyup', e =>{

    if (e.target.classList.contains('Enter')) {
        const pwCap = /[A-ZА-Я]/g.test(e.target.value);
        const pwLower = /[a-zа-я]/g.test(e.target.value);
        const pwChar = e.target.value.length >= 8;
        const pwNum = /[0-9]/g.test(e.target.value);
        const pwSpec = /[\[\]\\^$.|?*+()\/!@#%&\-_={},<>;:`~'"]/g.test(e.target.value);
        let pwCount = 0;

        if (pwSpec) {
            pwList.querySelector('#pwSpec').style.color = 'green';
            pwCount++;
        } else {
            pwList.querySelector('#pwSpec').style.color = '#aaa';

        }
        ;

        if (pwNum) {
            pwList.querySelector('#pwNum').style.color = 'green';
            pwCount++;
        } else {
            pwList.querySelector('#pwNum').style.color = '#aaa';
        }
        ;

        if (pwCap) {
            pwList.querySelector('#pwCap').style.color = 'green';
            pwCount++;
        } else {
            pwList.querySelector('#pwCap').style.color = '#aaa';
        }
        ;

        if (pwLower) {
            pwList.querySelector('#pwLower').style.color = 'green';
            pwCount++;
        } else {
            pwList.querySelector('#pwLower').style.color = '#aaa';
        }
        ;

        if (pwChar) {
            pwList.querySelector('#pwChar').style.color = 'green';
            pwCount++;
        } else {
            pwList.querySelector('#pwChar').style.color = '#aaa';
        }
        ;

        progressBarItem.forEach(el => {
            el.classList.remove('active');
        });
        progressBarText.textContent = 'Password is blank';
        progressBarItem[0].style.backgroundColor = '#FF4B47';
        progressBarItem[1].style.backgroundColor = '#FF4B47';
        progressBarItem[2].style.backgroundColor = '#F9AE35';
        progressBarItem[3].style.backgroundColor = '#F9AE35';
        progressBarItem[4].style.backgroundColor = '#2DAF7D';

        switch (pwCount) {
            case 0:
                progressBarItem.forEach(el => {
                    el.classList.remove('active');
                });
                progressBarText.textContent = 'Password is blank';
                break;
            case 1:
                progressBarItem[0].classList.add('active');
                progressBarText.textContent = '1 of 5 Too weak';
                break;
            case 2:
                progressBarItem[0].classList.add('active');
                progressBarItem[1].classList.add('active');
                progressBarText.textContent = '2 of 5 Too weak';
                break;
            case 3:
                progressBarItem[0].classList.add('active');
                progressBarItem[1].classList.add('active');
                progressBarItem[2].classList.add('active');
                progressBarText.textContent = '3 of 5 Could be stronger';
                break;
            case 4:
                progressBarItem[0].classList.add('active');
                progressBarItem[1].classList.add('active');
                progressBarItem[2].classList.add('active');
                progressBarItem[3].classList.add('active');
                progressBarText.textContent = '4 of 5 Could be stronger';
                break;
            case 5:
                progressBarItem[0].classList.add('active');
                progressBarItem[1].classList.add('active');
                progressBarItem[2].classList.add('active');
                progressBarItem[3].classList.add('active');
                progressBarItem[4].classList.add('active');
                progressBarText.textContent = '5 of 5 - Strong password';
                break;

        }
    }else {

    }
if ((inputWrapper[1].value.length === inputWrapper[0].value.length) && (inputWrapper[0].value.length > 0)){
    btn.disabled = false;
    btn.classList.add('btn-enable');
}else {
    btn.disabled = true;
    btn.classList.remove('btn-enable');
}

}));
